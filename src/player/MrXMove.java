package player;



import scotlandyard.*;
import swing.algorithms.Dijkstra;

import java.util.*;
import java.util.List;


public class MrXMove {

    protected ScotlandYardView view;
    protected Dijkstra d;
    private AIGameView aiGameView;

    public MrXMove(ScotlandYardView view, Dijkstra dij) {
        this.d = dij;
        this.view = view;
        this.aiGameView = new AIGameView(view);
    }

    private List<Colour> getDetectives() {              //Gets a list of all detectives in the game
        List<Colour> detectives = new ArrayList<>();
        for(Colour player : aiGameView.view.getPlayers()) {
            if (player != Colour.Black) {
                detectives.add(player);
            }
        }
        return detectives;
    }


    private List<Integer> findMoveWithNode(List<Move> moves, int node) {
        List<Integer> nodeList = new ArrayList<Integer>();                                       //Finds moves which go to a specific node
        for(Move move : moves) {
            if(move instanceof MoveTicket) {
                if( ((MoveTicket) move).target == node) {
                    nodeList.add(moves.indexOf(move));
                }
            }
            if(move instanceof MoveDouble) {
                if( ((MoveDouble) move).move1.target == node || ((MoveDouble) move).move2.target == node) {
                    nodeList.add(moves.indexOf(move));
                }
            }
        }
        return nodeList;
    }

    private Map<Transport, Integer> remainingTickets(Colour colour) {               //Finds how many tickets remain on a player
        Map<Transport, Integer> m = new HashMap<Transport, Integer>();
        m.put(Transport.Bus, aiGameView.view.getPlayerTickets(colour, Ticket.Bus));

        m.put(Transport.Taxi, aiGameView.view.getPlayerTickets(colour, Ticket.Taxi));
        m.put(Transport.Underground, aiGameView.view.getPlayerTickets(colour, Ticket.Underground));
        return m;
    }

    private void setMoveScore(int[] moveScores, List<Integer> listOfIndexes, int valToAdd) {         //Adds value to moves scores
        for (Integer node : listOfIndexes) {
            moveScores[node] = moveScores[node] + valToAdd;
        }

    }



    public int[] scoreMoves(int location, List<Move> moves) {
        int[] moveScores = new int[moves.size()];                   //Making nodes moving towards detectives negative
        List<Colour> detectives = getDetectives();
        int defcon = 3;

        for(Colour det : detectives) {
            List<Integer> quickestRoute = d.getRoute(aiGameView.view.getPlayerLocation(det), location, remainingTickets(det));
            if(quickestRoute.size() > 2) {
                int nodeToPlayer = quickestRoute.get(quickestRoute.size() - 2);
                if(quickestRoute.size() <= 4) {
                    List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                    setMoveScore(moveScores, listOfIndexes, -15);
                    defcon = 2;
                }
                else if(quickestRoute.size() <= 6) {
                    List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                    setMoveScore(moveScores, listOfIndexes, -10);
                }
                else {
                    List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                    setMoveScore(moveScores, listOfIndexes, -5);
                }
            }
            else {
                int nodeToPlayer = quickestRoute.get(1);
                List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                setMoveScore(moveScores,listOfIndexes, -50);
            }
        }

        System.out.println(moveScores);
        return moveScores;
    }
}
