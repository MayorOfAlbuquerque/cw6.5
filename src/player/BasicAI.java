package player;

import scotlandyard.*;
import swing.algorithms.Dijkstra;

import java.util.*;
import java.util.List;

/**
 * The RandomPlayer class is an example of a very simple AI that
 * makes a random move from the given set of moves. Since the
 * RandomPlayer implements Player, the only required method is
 * notify(), which takes the location of the player and the
 * list of valid moves. The return value is the desired move,
 * which must be one from the list.
 */
public class BasicAI implements Player {

    protected ScotlandYardView view;
    protected Dijkstra d;

    public BasicAI(ScotlandYardView view, String graphFilename) {
        this.view = view;
        this.d = new Dijkstra(graphFilename);


    }

    private int[] scores(int location, List<Move> moves) {
        if(moves.size() == 1) {                     //If only one move in List just return array of 1 element
            int[] onlyMove = new int[1];
            onlyMove[0] = 1;
            return onlyMove;
        }
        MrXMove xMoves = new MrXMove(view, d);
        int[] moveScores = new int[moves.size()];

        if(view.getCurrentPlayer() == Colour.Black) {
            moveScores = xMoves.scoreMoves(location, moves);
        }
        /*
        else {
            moveScores      move detectives if not a black player
        }*/


        return moveScores;
    }


    @Override
    public void notify(int location, List<Move> moves, Integer token, Receiver receiver) {

        int[] y = scores(location, moves);
        int x = -100;
        for(int i = 0; i < y.length; i++) {
            if(y[i] > x) {
                x = i;
            }
        }

        Move moveToPlay = moves.get(x);
        scores(location, moves);



        receiver.playMove(moveToPlay, token);
    }
}
