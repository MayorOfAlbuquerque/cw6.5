package player;

import scotlandyard.*;

import java.util.*;
import java.util.List;


public class AIGameView {

    protected ScotlandYardView view;
    protected Map<Colour, Integer> playerLocations;

    public AIGameView(ScotlandYardView view) {
        this.view = view;
        playerLocations = new HashMap<Colour, Integer>();
        List<Colour> players = view.getPlayers();
        for(Colour player : players) {
            playerLocations.put(player, view.getPlayerLocation(player));
        }
    }


    private void movePlayer(Colour player, int newLocation) {
        playerLocations.remove(player);
        playerLocations.put(player, newLocation);
    }

}
