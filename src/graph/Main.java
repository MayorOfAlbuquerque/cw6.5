// runs Prim's and Dijkstra's algorithms on graphs
package graph;
import graph.*;


public class Main {
  
  public static void main(String[] args) {

	//instances for loading and saving simple graph files
	Reader reader = new Reader();
	Writer writer = new Writer();
		
	//load input graph
	try {
	  reader.read("c:/data/graph.txt"); //change input path
	  System.out.println("Graph read.");
	} catch (Exception e) {}
		
	//calculate single-source-all-shortest-paths and output one route
	Graph<Integer,Integer> graph = reader.graph();
	DijkstraCalculator dijk = new DijkstraCalculator(graph);
	Graph<Integer,Integer> result = dijk.getResult(3,159);
				
	//output route as graph, use 'java -jar GraphViewer.jar route.txt' to view
	try {
	  writer.setGraph(result);
	  writer.write("c:/data/route.txt"); //change output path
	  System.out.println("Inverse route graph written.");
	} catch (Exception e) {}
	
	//calculate and output minimum spanning tree 
	PrimCalculator prim = new PrimCalculator(graph);
	Graph<Integer,Integer> result2 = prim.getResult(1);
				
	//output minimum spanning tree as graph, use 'java -jar GraphViewer.jar mst.txt' to view
	try {
	  writer.setGraph(result2);
	  writer.write("c:/data/mst.txt");
	  System.out.println("Minimum spanning tree written.");
	} catch (Exception e) {}
} }